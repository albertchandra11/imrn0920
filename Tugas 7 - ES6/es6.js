console.log(" Nomor 1 \n")
const golden = goldenFunction = () => {
    console.log('this is golden!!')
}
golden()

console.log('\n Nomor 2 \n')
const newFunction = function literal(firstName, lastName){
    return {
      firstName,
      lastName,
      fullName: function(){
        console.log(firstName + " " + lastName)
        return 
      }
    }
  }
  newFunction("William", "Imoh").fullName() 

console.log('\n Nomor 3 \n')
let newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
  }
const {firstName,lastName,destination,occupation} = newObject

console.log(firstName, lastName, destination, occupation)

console.log('\n Nomor 4 \n')
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
let combined = [...west,...east]

console.log(combined)

console.log('\n Nomor 5 \n')
const planet = "earth"
const view = "glass"
const before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`
 
console.log(before)