console.log(' Nomor 1 \n')
now = new Date()
thisyear = now.getFullYear()
function arrayToObject(input) {
    iD = {}
    for (var i=0;i<input.length; i++){
        iD.firstName = input[i][0]
        iD.lastName = input[i][1]
        iD.gender = input[i][2]
        if (thisyear > input[i][3]){
            iD.age = thisyear - input[i][3]
        } else{
            iD.age = 'Invalid Birth Year'
        }
        console.log(i+1 + '. '+input[i][0] + ' ' +input[i][1]+':')
        console.log(iD)
    }
} 
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people)
arrayToObject(people2)

console.log('\n Nomor 2 \n')
function shoppingTime(memberId, money) {
    if (memberId == null || memberId == ''){
        return 'Mohon maaf, toko X hanya berlaku untuk member saja'
    }
    else {
    receipt = {}
    receipt.memberId = memberId
    receipt.money = money
    listbelanja = []
    sisa = 0
    if (money >=1500000){
    listbelanja.push('Sepatu Stacattu','Baju Zoro', 'Baju H&N','Sweater Uniklooh', 'Casing Handphone')
    sisa = money - 2475000
    receipt.listbelanja = listbelanja
    receipt.changeMoney = sisa
    return receipt
    }
    else if (money >= 975000){
    listbelanja.push('Baju Zoro', 'Baju H&N', 'Sweater Uniklooh', 'Casing Handphone')
    sisa = money - 975000
    receipt.listbelanja = listbelanja
    receipt.changeMoney = sisa
    return receipt
    }
    else if (money >= 475000){
    listbelanja.push('Baju H&N', 'Sweater Uniklooh','Casing Handphone')
    sisa = money - 475000
    receipt.listbelanja = listbelanja
    receipt.changeMoney = sisa
    return receipt
    }
    else if (money >= 225000){
    listbelanja.push('Sweater Uniklooh', 'Casing Handphone')
    sisa = money - 225000
    receipt.listbelanja = listbelanja
    receipt.changeMoney = sisa
    return receipt
    }
    else if (money >= 50000){
    listbelanja.push('Casing Handphone')
    sisa = money - 50000
    receipt.listbelanja = listbelanja
    receipt.changeMoney = sisa
    return receipt
    }
    else if (money <50000){
        return 'Mohon maaf, uang tidak cukup'
    }
}
  }
  // TEST CASES
  console.log(shoppingTime('1820RzKrnWn08', 2475000));
    //{ memberId: '1820RzKrnWn08',
    // money: 2475000,
    // listPurchased:
    //  [ 'Sepatu Stacattu',
    //    'Baju Zoro',
    //    'Baju H&N',
    //    'Sweater Uniklooh',
    //    'Casing Handphone' ],
    // changeMoney: 0 }
  console.log(shoppingTime('82Ku8Ma742', 170000));
  //{ memberId: '82Ku8Ma742',
  // money: 170000,
  // listPurchased:
  //  [ 'Casing Handphone' ],
  // changeMoney: 120000 }
  console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
  console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
  console.log(shoppingTime()); //Mohon maaf, toko X hanya berlaku untuk member saja

console.log ('\n Nomor 3 \n')
function naikAngkot(arrPenumpang){
    rute = ['A', 'B', 'C', 'D', 'E', 'F']
    rec1 = {}
    rec2 = {}
    receipt1 = []
    awal1 = rute.indexOf(arrPenumpang[0][1])
    end1 = rute.indexOf(arrPenumpang[0][2])
    awal2 = rute.indexOf(arrPenumpang[1][1])
    end2 = rute.indexOf(arrPenumpang[1][2])
    rec1.penumpang = arrPenumpang[0][0]
    rec1.naikDari = arrPenumpang[0][1]
    rec1.tujuan = arrPenumpang[0][2]
    rec1.bayar = (end1-awal1)*2000
    rec2.penumpang = arrPenumpang[1][0]
    rec2.naikDari = arrPenumpang[1][1]
    rec2.tujuan = arrPenumpang[1][2]
    rec2.bayar = (end2-awal2)*2000
    receipt1.push(rec1,rec2)
    return receipt1
}
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]