console.log(' Nomor 1 \n')
// Jawaban Nomor 1
console.log(' LOOPING PERTAMA \n')
var angka = 2
while (angka <= 20) {
    console.log (angka, ' - I love coding')
    angka +=2
}
console.log('\n LOOPING KEDUA \n')
var Angka = 20
while (Angka >= 2) {
    console.log (Angka, ' - I will become a mobile developer')
    Angka -= 2
}

console.log('\n Nomor 2 \n')
for (var angka2 = 1; angka2 <= 20 ; angka2 ++ ) {
    if (angka2%2 ==0){
        console.log (angka2, ' - Berkualitas')
    }
    else if (angka2%3 == 0) {
        console.log (angka2, ' - I love coding') 
    }
    else {
        console.log (angka2, ' - Santai')
    }
    }        

console.log('\n Nomor 3 \n')
for (var angka3 = 1; angka3 <=4 ; angka3 ++){
    console.log('########')
}

console.log('\n Nomor 4 \n')
for (var angka4 = '#'; angka4.length <=7 ; angka4 += '#'){
    console.log(angka4)
}

console.log('\n Nomor 5 \n')
for (var angka5 = 1; angka5 <=8 ; angka5 ++){
    if (angka5%2 ==0){
        console.log(' # # # #')
    }
    else {
        console.log('# # # #')
    }
}