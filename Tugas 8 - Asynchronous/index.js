// di index.js
var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Tulis code untuk memanggil function readBooks di sini
var waktu = 100
readBooks(waktu, books[0], function(sisaWaktu){
    if (waktu != sisaWaktu){
    waktu = sisaWaktu
    readBooks(waktu,books[1],function(sisaWaktu){
        if (waktu != sisaWaktu){
        waktu = sisaWaktu
        readBooks(waktu,books[2],function(sisaWaktu){
            waktu = sisaWaktu
        })
    }})   
}}
)

// done