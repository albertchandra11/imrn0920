var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise 
var i = 0
var time = 10000
function readBook(){
    readBooksPromise(time, books[i]).then((fulfilled)=>{
        if(i != books.length -1){
            time = fulfilled
            i++
            readBook()
        }
    })
    .catch((error)=>{
        console.log(`kurang waktu: ${error} ms`)
    })
}

readBook()
//end