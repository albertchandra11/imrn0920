import React, {Component} from 'react';
import { StyleSheet, Text, View, Platform, Image, TouchableOpacity, FlatList } from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import Skills from './skillData.json'
import SkillData from './skilldata'

export default class Skill extends Component{
    render(){
        return(
            <View style={styles.container}>
                <View style={styles.logo}>
                    <Image source = {require('./images/logo.png')} style={{height:51, width:187.5}}/>
                </View>
                <View style={styles.profile}>
                    <Image source = {require('./images/logo2.png')} style={{height: 26, width: 26}}/>
                    <View style={styles.textProfile}>
                    <Text style={styles.textHai}>Hai</Text>
                    <Text style={styles.textName}>Habata I Tala</Text>
                    </View>
                </View>
                <View style={styles.header}>
                    <Text style={styles.textSkill}>SKILL</Text>
                    <View style={styles.port}></View>
                </View>
                <View style={styles.category}>
                    <View style={styles.catBox}>
                        <Text style={styles.catText}>Library / Framework</Text>
                    </View>
                    <View style={styles.catBox1}>
                        <Text style={styles.catText}>Bahasa Pemrograman</Text>
                    </View>
                    <View style={styles.catBox2}>
                        <Text style={styles.catText}>Library</Text>
                    </View>
                </View>
                <FlatList
                data={Skills.items}
                renderItem={(skill)=><SkillData skill={skill.item}/>}
                keyExtractor={(item)=>item.id}
                ItemSeparatorComponent={()=><View style={{height:0.5, backgroundColor:'#E5E5E5'}}/>}
                />
            </View>

        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1,
    },
    logo:{
        alignItems: 'flex-end'
    },
    profile:{
        paddingTop: 7,
        paddingLeft: 19,
        flexDirection: 'row'
    },
    textProfile:{
        paddingLeft: 11
    },
    textHai:{
        fontSize: 12
    },
    textName:{
        fontSize: 16
    },
    header:{
        paddingHorizontal: 16,
    },
    textSkill:{
        fontSize: 36
    },
    port:{
        borderWidth: 4,
        borderColor:'#3ec6ff'
    },
    category:{
        alignItems: 'center',
        paddingHorizontal: 16,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    catBox:{
        height: 32,
        width: 125,
        marginTop: 10,
        backgroundColor: '#3ec6ff',
        borderRadius: 8
    },
    catBox1:{
        height: 32,
        width: 136,
        backgroundColor: '#3ec6ff',
        borderRadius: 8,
        marginTop: 10
    },
    catBox2:{
        height: 32,
        width: 70,
        backgroundColor: '#3ec6ff',
        borderRadius: 8,
        marginTop: 10
    },
    catText:{
        fontSize: 12,
        color: '#003366',
        fontWeight: "bold",
        alignSelf: "center",
        paddingTop: 9
    }
})