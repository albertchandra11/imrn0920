import React, {Component} from 'react';
import { StyleSheet, Text, View, Platform, Image, TouchableOpacity, FlatList } from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { render } from 'react-dom';

export default class SkillData extends Component{
    render(){
        let skill= this.props.skill
        return(
            <View style={styles.container}>
                <View style={styles.bigBox}>
                    <View style={styles.icon}>
                    <MaterialCommunityIcons name={skill.iconName}size={100} color='#003366'/>
                    </View>
                    <View style={styles.titles}>
                        <Text style={styles.textTitle}>{skill.skillName}</Text>
                        <Text style={styles.textCat}>{skill.categoryName}</Text>
                        <Text style={styles.textPercent}>{skill.percentageProgress}</Text>
                    </View>
                    <MaterialCommunityIcons name='chevron-right' size={80} color='#003366'/>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1
    },
    bigBox:{
        marginLeft: 14,
        marginTop: 10,
        marginRight: 18,
        height: 129,
        borderRadius: 8,
        backgroundColor: '#b4e9ff',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems:'center',
        shadowOffset: {width: 1, height: 4},
        elevation: 4
    },
    icon:{
        paddingLeft: 10.5
    },
    textTitle:{
        fontSize: 24,
        fontWeight: "bold",
        color:'#003366',
        paddingTop: 13,
        alignSelf:'flex-start'
    },
    textCat:{
        fontSize: 16,
        fontWeight: "bold",
        color: '#3ec6ff',
        paddingTop: 5,
        alignSelf:'flex-start'
    },
    textPercent:{
        fontSize: 48,
        fontWeight: "bold",
        color: '#ffffff',
        alignSelf: 'flex-end'
    },
    titles:{
        width: 180,
        height: 120,
        paddingLeft: 25.5,
        paddingBottom: 10
    }
})