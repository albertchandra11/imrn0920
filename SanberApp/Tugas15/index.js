import React from 'react'
import { StyleSheet, Text, View } from 'react-native';

import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';
import Login from './loginScreen';
import Profile from './aboutScreen';
import Skill from './skillScreen'
import Proyek from './projectScreen'
import Tambah from './addScreen'





const Tab = createBottomTabNavigator();
const Drawer = createDrawerNavigator();
const Stack = createStackNavigator();

const TabsScreen = () =>(
    <Tab.Navigator>
        <Drawer.Screen name="Profile" component={Profile}/>
        <Tab.Screen name ='Skill' component={Skill}/>
        <Tab.Screen name ='Project' component={Proyek}/>
        <Tab.Screen name ='Tambah' component={Tambah}/>
      </Tab.Navigator>
  )

const index = () => (
        <NavigationContainer >
            <Stack.Navigator>
                <Stack.Screen name="Login" component={Login} />
                <Stack.Screen name="Profile" component={Profile} />
                <Stack.Screen name="Skill" component={Skill} />
                <Stack.Screen name="Proyek" component={Proyek} />
                <Stack.Screen name="Tambah" component={Tambah} />
                <Stack.Screen name="MainApp" component={MainApp} />
                <Stack.Screen name="MyDrawwer" component={MyDrawwer}/>
            </Stack.Navigator>
      </NavigationContainer>
    )

const MyDrawwer = ()=>(
    <NavigationContainer initialRouteName={Login}>
    <Drawer.Navigator>
        <Stack.Screen name="Login" component={Login} />
        <Drawer.Screen name="Profile" component={TabsScreen} />
    </Drawer.Navigator>
    </NavigationContainer>
    )

export default MyDrawwer

const styles = StyleSheet.create({})
