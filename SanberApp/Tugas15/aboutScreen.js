import React, {Component} from 'react';
import { StyleSheet, Text, View, Platform, Image, TouchableOpacity, FlatList } from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons';
export default class App extends Component {
    render(){
      return(
        <View style={styles.container}>
          <View style={styles.header}>
            <Text style={styles.about}>Tentang Saya</Text>
            <Image source = {require('./images/logo2.png')} style={{height: 200, width: 200}} />
            <Text style={styles.name}>Habata I Tala</Text>
            <Text style={styles.job}>React Native Developer</Text>
            <View style={styles.box1}>
              <Text style={styles.port}>Portofolio</Text>
              <View style={styles.garishead}>
              </View>
              <View style={styles.applikasi}>
                <View style={styles.icons}>
                <MaterialCommunityIcons name="gitlab" size={40} color="#3EC6FF" />
                  <Text style={styles.iconTitle}>@habata_ta_ta</Text>  
                </View>
                <View style={styles.icons}>
                <MaterialCommunityIcons name="github-circle" size={40} color="#3EC6FF" />
                  <Text style={styles.iconTitle}>@habata_ta_ta</Text>
                </View>
              </View>
            </View>
            <View style={styles.box2}>
              <Text style={styles.port}>Hubungi Saya</Text>
              <View style={styles.garishead}>
              </View>
              <View style={styles.socmed}>
                <View style={styles.icons2}>
                  <MaterialCommunityIcons name="facebook-box" size={40} color="#3EC6FF" />
                  <Text style={styles.iconTitle}>@habata_ta_ta</Text>
                </View>  
                <View style={styles.icons2}>
                  <MaterialCommunityIcons name="instagram" size={40} color="#3EC6FF" />
                  <Text style={styles.iconTitle}>@habata_ta_ta</Text>
                </View>
                <View style={styles.icons2}>
                  <MaterialCommunityIcons name="twitter" size={40} color="#3EC6FF" />
                  <Text style={styles.iconTitle}>@habata_ta_ta</Text>
                </View>
              </View>
            </View>
          </View>
        </View>
      )
    }
}

const styles = StyleSheet.create({
  container:{
    flex: 1,
    paddingBottom: 10
  },
  header:{
    alignContent: "center",
    padding: 64,
    alignItems: "center",
  },
  about:{
    fontSize: 36,
    fontWeight: "bold",
    color: '#003366',
    paddingBottom: 12
  },
  name:{
    fontSize: 24,
    color: '#003366',
    paddingTop: 24
  },
  job:{
    fontSize: 16,
    color: '#3EC6FF',
    paddingTop: 8,
    paddingBottom: 16
  },
  box1:{
    height: 140,
    width: 359,
    paddingHorizontal: 8,
    backgroundColor: '#EFEFEF',
    borderRadius: 16,
    marginHorizontal: 8
  },
  port:{
    fontSize: 18,
    color: '#003366',
    paddingTop: 5,
    paddingLeft: 8,
    paddingBottom: 8,
  },
  garishead:{
    borderWidth:1,
    borderColor: '#003366',
  },
  icons:{
    paddingTop: 20,
    alignItems: 'center',
    justifyContent: 'center'
  },
  iconTitle:{
    fontSize: 16,
    color: '#003366',
    padding: 10,
  },
  applikasi:{
    flexDirection: 'row',
    justifyContent: "space-around"
  },
  box2:{
    height: 251,
    width: 359,
    paddingHorizontal: 8,
    backgroundColor: '#EFEFEF',
    borderRadius: 16,
    marginTop: 9,
    marginHorizontal: 8
  },
  socmed:{
    justifyContent: 'space-around',
    alignItems: 'center'
  },
  icons2:{
    paddingTop: 20,
    alignItems: "center",
    flexDirection: "row"
  },
})