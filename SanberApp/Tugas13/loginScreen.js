import React, {Component} from 'react';
import { StyleSheet, Text, View, Platform, Image, TouchableOpacity, FlatList } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons'
import Toggle from './aboutScreen'
export default class App extends Component {

    render(){
        return(
            <View style={styles.container}>
                <View style={styles.logo}>
                    <Image source = {require('./images/logo.png')} style={{width: 375, height:102}}/>
                    <Text style={styles.headerText}>Login</Text>
                </View>
                <View style={styles.boxex}>
                    <Text style={styles.boxText}>Username / Email</Text>
                    <View style={styles.boxIsi}>
                    </View>
                    <Text style={styles.boxText}>Password</Text>
                    <View style={styles.boxIsi}>
                    </View>
                </View>
                <View style={styles.button}>
                    <View style={styles.buttonpPress}>
                    <Text style={styles.reg}>Daftar</Text>
                    </View>
                    <Text style={styles.or}>atau</Text>
                    <View style={styles.login}>
                    <Text style={styles.logIn}>Masuk?</Text>
                    </View>
                </View>

            </View>
            
        )
    }
}
const styles = StyleSheet.create({
  container: {
      flex: 1,
      paddingTop: 63,
      alignSelf: "stretch"
  },
  logo:{
      alignItems: "center",
      justifyContent: "center"
  },
  headerText:{
      fontSize: 24,
      paddingTop: 70,
      color: '#003366'
  },
  boxex:{
      paddingHorizontal: 40,
      paddingTop: 30,
      color: '#003366'
  },
  boxText:{
      color: '#003366',
      padding: 4,
      fontSize: 16,
  },
  boxIsi:{
      height: 48,
      width: 294,
      borderColor: '#003366',
      borderWidth: 1,
      paddingHorizontal: 60
  },
  button:{
      padding: 40,
      alignItems: "center",
      justifyContent: 'center'
  },
  buttonpPress:{
      height: 40,
      width: 140,
      backgroundColor: '#003366',
      borderRadius: 16,
      justifyContent: 'center',
      alignItems: 'center'
  },
  reg:{
      color: 'white',
      justifyContent: 'center',
      fontSize: 24,
      fontWeight: "normal"
  },
  or:{
      color: '#3EC6ff',
      padding: 15,
      fontSize: 24
  },
  login:{
    height: 40,
    width: 140,
    backgroundColor: '#3EC6ff',
    borderRadius: 16,
    justifyContent: 'center',
    alignItems: 'center'
  },
  logIn:{
    color: 'white',
    justifyContent: 'center',
    fontSize: 24,
    fontWeight: "normal",
  }
})