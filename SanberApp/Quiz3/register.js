import React, { Component } from 'react'
import { StyleSheet, Text, View, TextInput, Button,Image } from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { FontAwesome } from '@expo/vector-icons';
import { color } from 'react-native-reanimated';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import Login from './login'

const Register =({navigation}) =>{
        return(
            <ScrollView>
            <View style={styles.container}>
                <View style={styles.statusBar}>
                    <Text style={styles.clock}>9:41</Text>
                    <View style={styles.topIcons}>
                        <MaterialCommunityIcons name='signal' size={15}/>
                        <Text> </Text>
                        <MaterialCommunityIcons name='wifi' size={15}/>
                        <Text> </Text>
                        <FontAwesome name="battery-full" size={15}/>
                    </View>
                </View>
                <View style={styles.body}>
                    <Text style={styles.Welcome}>Welcome</Text>
                    <Text style={styles.subWelc}>Sign up to continue</Text>
                </View>
                <View style={styles.boxIsi}>
                    <Text style={styles.isiJudul}>Name</Text>
                    <Text style={styles.isiSubjudul}>Shakibul islam</Text>
                        <View style={styles.garisbawah}></View>
                    <Text style={styles.isiJudul}>Email</Text>
                    <Text style={styles.isiSubjudul}>shakibulislam402@gmail.com</Text>
                        <View style={styles.garisbawah}></View>
                    <Text style={styles.isiJudul}>Phone number</Text>
                    <Text style={styles.isiSubjudul}>+44 213 032 578</Text>
                        <View style={styles.garisbawah}></View>
                    <Text style={styles.isiJudul}>Password</Text>
                    <Text style={styles.isiSubjudul}>************</Text>
                        <View style={styles.garisbawah}></View>
                    <TouchableOpacity onPress={() => navigation.navigate('Login')} style={styles.regButton}>
                        <Text style={styles.regText}>Sign Up</Text>
                    </TouchableOpacity>
                    <View style={styles.logText}>
                    <Text style={styles.logText1}>Already have an account?</Text>
                    <Text onPress={()=> navigation.navigate('Login')} style={styles.logText2}> Sign In</Text>
                    </View>
                </View>
                <View style={styles.lineBar}>
                </View>
            </View>
            </ScrollView>
        )
}

export default Register
const styles =StyleSheet.create ({
    container:{
        flex: 1,
    },
    statusBar:{
        height:20,
        flexDirection:'row',
        justifyContent: 'space-between',
        paddingHorizontal: 20
    },
    clock:{
        fontSize:15,
        fontStyle: 'italic',
    },
    topIcons:{
        flexDirection:'row'
    },
    body:{
        flex: 1,
        marginTop: 164,
        marginHorizontal: 22,
    },
    Welcome:{
        fontSize: 30,
        fontWeight: "bold",
        elevation: 5,
        textShadowOffset: {width: 0,height: 1},
        textShadowRadius: 5,
        textShadowColor:'#000020'
    },
    subWelc:{
        fontSize: 12,
        color:'#4D4D4D',
    },
    boxIsi:{
        width: 366,
        height: 536,
        borderRadius: 11,
        backgroundColor: '#ffffff',
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.8,
        shadowRadius: 1, 
        elevation: 2,
        alignSelf: "center",
        paddingLeft: 16,
        paddingTop: 72,
        paddingRight: 32
    },
    isiJudul:{
        fontSize: 12,
        color: '#4D4D4D',
    },
    isiSubjudul:{
        fontSize: 15,
        color: '#4C475A',
        paddingTop: 12
    },
    garisbawah:{
        paddingTop: 8,
        borderColor: '#E6EAEE',
        borderTopWidth: 1,
        paddingBottom: 31
    },
    regButton:{
        height: 50,
        backgroundColor: '#F77866',
        borderRadius: 6
    },
    regText:{
        fontSize: 14,
        color: '#ffffff',
        textAlign: "center",
        paddingVertical: 16
    },
    logText:{
        flexDirection: 'row',
        justifyContent: 'center'
    },
    logText1:{
        fontSize: 12,
        color: '#4D4D4D',
        paddingTop: 11
    },
    logText2:{
        fontSize: 12,
        color: '#F77866',
        paddingTop: 11
    },
    lineBar:{
        width: 134,
        height: 5,
        backgroundColor: '#000000',
        borderRadius: 2.5,
        alignSelf: 'center',
        marginVertical: 10
    }
})