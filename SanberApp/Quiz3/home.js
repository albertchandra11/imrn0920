import React, { Component } from 'react'
import { StyleSheet, Text, View, TextInput, Button,Image } from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { FontAwesome } from '@expo/vector-icons';
import { color } from 'react-native-reanimated';
import { ScrollView } from 'react-native-gesture-handler';

const HomeScreen =({navigation}) =>{
        return(
            <ScrollView>
            <View style={styles.container}>
                <View style={styles.statusBar}>
                    <Text style={styles.clock}>9:41</Text>
                    <View style={styles.topIcons}>
                        <MaterialCommunityIcons name='signal' size={15}/>
                        <Text> </Text>
                        <MaterialCommunityIcons name='wifi' size={15}/>
                        <Text> </Text>
                        <FontAwesome name="battery-full" size={15}/>
                    </View>
                </View>
                <View style={styles.searchBar}>
                    <View style={styles.searchBox}>
                        <FontAwesome name='search' size={16} color='#727C8E'/>
                        <Text> Search Product</Text>
                        <Text>              </Text>
                        <Text> |</Text>
                        <MaterialCommunityIcons name='camera-outline' size={16}/>
                    </View>
                    <FontAwesome style={styles.bell} name='bell-o' size={20}/>
                </View>
                <View style={styles.afterHeader}>
                    <Image source={require('./images/Slider.png')} style={{width: 382, height: 190}}/>
                </View>
                <View style={styles.beforeFlash}>
                    <View style={styles.bfitem}>
                    <Image source={require('./images/Man.png')} style={{width: 52, height: 52}}/>
                    <Text style={styles.bftext}>Man</Text>
                    </View>
                    <View style={styles.bfitem}>
                    <Image source={require('./images/Woman.png')} style={{width: 52, height: 52}}/>
                    <Text style={styles.bftext}>Woman</Text>
                    </View>
                    <View style={styles.bfitem}>
                    <Image source={require('./images/Kids.png')} style={{width: 52, height: 52}}/>
                    <Text style={styles.bftext}>Kids</Text>
                    </View>
                    <View style={styles.bfitem}>
                    <Image source={require('./images/Home.png')} style={{width: 52, height: 52}}/>
                    <Text style={styles.bftext}>More</Text>
                    </View>
                    <View style={styles.bfitem}>
                    <Image source={require('./images/More.png')} style={{width: 52, height: 52}}/>
                    <Text style={styles.bftext}>More</Text>
                    </View>
                </View>
                <View style={styles.flashsale}>
                    <View style={styles.Title}>
                    <Text style={styles.flashTitle}>Flash Sell</Text>
                    <Text style={styles.flashTime}>   03.30.30</Text>
                    </View>
                    <View style={styles.flashAll}>
                        <Text style={styles.All}>All</Text>
                        <FontAwesome name='chevron-right' size={10} color='#F89C52'/>
                    </View>
                </View>
                <View style={styles.isiFlash}>
                    <View style={styles.itemFlash}>
                        <Image source={require('./images/Rectangle.png')} style={{height: 117, width: 120}}/>
                        <Text style={styles.itemTitle}> Tiare Handwash</Text>
                        <Text style={styles.itemPrice}> $12.22</Text>
                    </View>
                    <View style={styles.itemFlash}>
                        <Image source={require('./images/Speak1.png')} style={{height: 117, width: 120}}/>
                        <Text style={styles.itemTitle}> JBL Speaker</Text>
                        <Text style={styles.itemPrice}> $12.22</Text>
                    </View>
                    <View style={styles.itemFlash}>
                        <Image source={require('./images/Speak2.png')} style={{height: 117, width: 120}}/>
                        <Text style={styles.itemTitle}> Google Home</Text>
                        <Text style={styles.itemPrice}> $80.30</Text>
                    </View>
                </View>
                <View style={styles.flashsale}>
                    <View style={styles.Title}>
                    <Text style={styles.flashTitle}>New Product</Text>
                    </View>
                    <View style={styles.flashAll}>
                        <Text style={styles.All}>All</Text>
                        <FontAwesome name='chevron-right' size={10} color='#F89C52'/>
                    </View>
                </View>
                <View style={styles.NP}>
                    <View style={styles.NPItems}>
                    <Image source={require('./images/shoe.png')} style={{height: 150, width: 183}}/>
                    </View>
                    <View style={styles.NPItems}>
                    <Image source={require('./images/chair.png')} style={{height: 150, width: 183}}/>
                    </View>
                </View>
                <View style={styles.lineBar}>
                </View>
            </View>
            </ScrollView>
        )
    
}

export default HomeScreen
const styles =StyleSheet.create ({
    container:{
        flex: 1,
    },
    statusBar:{
        height:20,
        flexDirection:'row',
        justifyContent: 'space-between',
        paddingHorizontal: 20
    },
    clock:{
        fontSize:15,
        fontStyle: 'italic',
    },
    topIcons:{
        flexDirection:'row'
    },
    searchBar:{
        marginTop: 24,
        marginHorizontal: 16,
        flexDirection: 'row',
        justifyContent: "center",
        marginBottom: 16
    },
    searchBox:{
        borderColor: '#727C8E',
        borderRadius: 11,
        height: 44,
        width: 352,
        borderWidth: 1,
        justifyContent: 'space-between',
        paddingLeft: 30,
        paddingRight: 16,
        paddingTop: 15,
        flexDirection: 'row'
    },
    bell:{
        paddingTop: 15,
        paddingLeft: 12
    },
    afterHeader:{
        flex: 1,
        alignItems:'center'
    },
    beforeFlash:{
        justifyContent: 'space-around',
        flexDirection: 'row',
        marginTop: 21,
        marginHorizontal: 16.7,
        marginBottom: 23
    },
    bfitem:{
        justifyContent: "center",
        alignItems: 'center'
    },
    bftext:{
        fontSize: 18,
        paddingTop: 8
    },
    flashsale:{
        justifyContent: 'space-between',
        marginHorizontal: 11,
        marginTop: 23,
        flexDirection: 'row'
    },
    Title:{
        flexDirection:'row',
    },
    flashTitle:{
        fontSize: 24,
        fontWeight: "bold"
    },
    flashTime:{
        fontSize: 12,
        color:'#F89C52',
    },
    flashAll:{
        flexDirection:'row',
        alignItems: "center"
    },
    All:{
        fontSize: 15
    },
    isiFlash:{
        marginHorizontal: 11,
        justifyContent: "center",
        flexDirection: 'row'
    },
    itemFlash:{
        width: 120,
        height: 170,
        backgroundColor: '#ffffff',
        borderRadius: 5,
        elevation: 5,
        textShadowOffset: {width: 0,height: 1},
        textShadowRadius: 5,
        textShadowColor:'#000020',
        marginHorizontal: 5
    }, 
    itemTitle:{
        paddingLeft: 10,
        paddingTop: 7,
        fontSize: 12,
        color: '#575757'
    },
    itemPrice:{
        paddingLeft: 10,
        paddingTop:3,
        fontSize: 12,
        color: '#323232',
        fontWeight: "bold"
    },
    lineBar:{
        width: 134,
        height: 5,
        backgroundColor: '#000000',
        borderRadius: 2.5,
        alignSelf: 'center',
        marginVertical: 10
    },
    NP:{
        marginHorizontal: 11,
        justifyContent: "center",
        flexDirection: 'row'
    },
    NPItems:{
        width: 183,
        height: 212,
        backgroundColor: '#ffffff',
        borderRadius: 5,
        elevation: 5,
        textShadowOffset: {width: 0,height: 1},
        textShadowRadius: 5,
        textShadowColor:'#000020',
        marginHorizontal: 5
    }
})