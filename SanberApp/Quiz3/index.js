import React from 'react'

import { createStackNavigator } from '@react-navigation/stack'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { NavigationContainer } from '@react-navigation/native'

import HomeScreen from './home'
import Login from './login'
import Register from './register'
import Splash from './splash'
import Cart from'./cart'
import Profile from './profile'
import Message from './message'

const Tab = createBottomTabNavigator()
const Stack = createStackNavigator()

const index = () => {
    return (
        <NavigationContainer >
            <Stack.Navigator initialRouteName={Splash}>
                <Stack.Screen name="Splash" component={Splash}/>
                <Stack.Screen name="Login" component={Login} />
                <Stack.Screen name="Register" component={Register} />
                <Stack.Screen name="Home" component={HomeScreen} />
                <Stack.Screen name="MainApp" component={MainApp} />
            </Stack.Navigator>
        </NavigationContainer>
    )
}

const MainApp = () => {
    return(
        <NavigationContainer>
        <Tab.Navigator>
            <Tab.Screen name="Home" component={HomeScreen} />
            <Tab.Screen name="Cart" component={Cart}/>
            <Tab.Screen name="Message" component={Message}/>
            <Tab.Screen name="Profile" component={Profile}/>
        </Tab.Navigator>
        </NavigationContainer>
    )
}

export default index