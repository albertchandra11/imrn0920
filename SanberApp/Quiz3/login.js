import React, { Component } from 'react'
import { StyleSheet, Text, View, TextInput, Button,Image } from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { FontAwesome } from '@expo/vector-icons';
import { color } from 'react-native-reanimated';
import { ScrollView } from 'react-native-gesture-handler';

const Login =({navigation}) =>{
        return(
            <ScrollView>
            <View style={styles.container}>
                <View style={styles.statusBar}>
                    <Text style={styles.clock}>9:41</Text>
                    <View style={styles.topIcons}>
                        <MaterialCommunityIcons name='signal' size={15}/>
                        <Text> </Text>
                        <MaterialCommunityIcons name='wifi' size={15}/>
                        <Text> </Text>
                        <FontAwesome name="battery-full" size={15}/>
                    </View>
                </View>
                <View style={styles.body}>
                    <Text style={styles.Welcome}>Welcome Back</Text>
                    <Text style={styles.subWelc}>Sign in to continue</Text>
                </View>
                <View style={styles.boxIsi}>
                    <Text style={styles.isiJudul}>Email</Text>
                    <Text style={styles.isiSubjudul}>shakibulislam402@gmail.com</Text>
                        <View style={styles.garisbawah}></View>
                    <Text style={styles.isiJudul}>Password</Text>
                    <View style={styles.password}>
                    <Text style={styles.isiSubjudul}>************</Text>
                    <MaterialCommunityIcons name='eye' size={15}/>
                    </View>
                        <View style={styles.garisbawah}></View>
                    <Text style={styles.forgot}>Forgot Password?</Text>
                    <View style={styles.regButton}>
                        <Text onPress={()=> navigation.navigate('Register')} style={styles.regText}>Sign In</Text>
                    </View>
                    <Text style={styles.or}>-OR-</Text>
                    <View style={styles.logins}>
                        <View style={styles.loginchoice}>
                            <MaterialCommunityIcons name='facebook-box' size={16} color='#3B5998' />
                            <Text style={styles.choices}>    Facebook</Text>
                        </View>
                        <View style={styles.loginchoice}>
                            <Image source ={require('./images/003-search.png')} style={{height:16, width: 16}}/>
                            <Text style={styles.choices}>     Google</Text>
                        </View>
                    </View>
                </View>
                <View style={styles.lineBar}>
                </View>
            </View>
            </ScrollView>
        )
}
export default Login
const styles =StyleSheet.create ({
    container:{
        flex: 1,
    },
    statusBar:{
        height:20,
        flexDirection:'row',
        justifyContent: 'space-between',
        paddingHorizontal: 20
    },
    clock:{
        fontSize:15,
        fontStyle: 'italic',
    },
    topIcons:{
        flexDirection:'row'
    },
    body:{
        flex: 1,
        marginTop: 153,
        marginHorizontal: 24,
    },
    Welcome:{
        fontSize: 30,
        fontWeight: "bold",
        color: '#0C0423'
    },
    subWelc:{
        fontSize: 12,
        color:'#4D4D4D',
    },
    boxIsi:{
        width: 366,
        height: 536,
        borderRadius: 11,
        backgroundColor: '#ffffff',
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.8,
        shadowRadius: 1, 
        elevation: 2,
        alignSelf: "center",
        paddingLeft: 16,
        paddingTop: 72,
        paddingRight: 32,
        marginTop: 31
    },
    isiJudul:{
        fontSize: 12,
        color: '#4D4D4D',
    },
    isiSubjudul:{
        fontSize: 15,
        color: '#4C475A',
        paddingTop: 12
    },
    garisbawah:{
        paddingTop: 8,
        borderColor: '#E6EAEE',
        borderTopWidth: 1,
        paddingBottom: 31
    },
    password:{
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingRight: 19
    },
    forgot:{
        fontSize: 12,
        color: '#0C0423',
        alignSelf: 'flex-end',
        paddingBottom: 50,
        fontWeight: 'bold'
    },
    regButton:{
        height: 50,
        backgroundColor: '#F77866',
        borderRadius: 6
    },
    regText:{
        fontSize: 14,
        color: '#ffffff',
        textAlign: "center",
        paddingVertical: 16
    },
    or:{
        fontSize: 15,
        color: '#4C475A',
        alignSelf: 'center',
        paddingVertical: 31,
        fontWeight: 'bold'
    },
    logins:{
        flexDirection: 'row',
        justifyContent: "space-between"
    },
    loginchoice:{
        width: 149,
        height: 44,
        borderWidth: 1,
        borderColor: '#E6EAEE',
        borderRadius: 6,
        paddingVertical: 13,
        paddingHorizontal: 20,
        flexDirection: 'row',
        justifyContent: "flex-start"
    },
    choices:{
        color : '#4D4D4D',
        fontSize: 14,
        fontWeight: "bold"
    },
    lineBar:{
        width: 134,
        height: 5,
        backgroundColor: '#000000',
        borderRadius: 2.5,
        alignSelf: 'center',
        marginVertical: 10
    }
})