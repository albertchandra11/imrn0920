import React, { Component } from 'react'
import { StyleSheet, Text, View, TextInput, Button,Image } from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { FontAwesome } from '@expo/vector-icons';
import { color } from 'react-native-reanimated';
import { ScrollView } from 'react-native-gesture-handler';

const Message =({navigation}) =>{
        return(
            <ScrollView>
                <View style={styles.container}>
                <View style={styles.statusBar}>
                    <Text style={styles.clock}>9:41</Text>
                    <View style={styles.topIcons}>
                        <MaterialCommunityIcons name='signal' size={15}/>
                        <Text> </Text>
                        <MaterialCommunityIcons name='wifi' size={15}/>
                        <Text> </Text>
                        <FontAwesome name="battery-full" size={15}/>
                    </View>
                </View>
                <View style={styles.body}>
                    <Text>Message</Text>
                </View>
                <View style={styles.lineBar}>
                </View>
            </View>
            </ScrollView>
        )
    
}
export default Message
const styles =StyleSheet.create ({
    container:{
        flex: 1,
    },
    statusBar:{
        height:20,
        flexDirection:'row',
        justifyContent: 'space-between',
        paddingHorizontal: 20
    },
    clock:{
        fontSize:15,
        fontStyle: 'italic',
    },
    topIcons:{
        flexDirection:'row'
    },
    body:{
        flex: 1
    },
    circle:{
        width: 305,
        height: 305,
        borderRadius: 180,
        backgroundColor: '#211F6510',
        alignSelf: 'center',
        marginTop: 279,
        justifyContent: 'center',
        alignItems:'center',
    }, 
    lineBar:{
        width: 134,
        height: 5,
        backgroundColor: '#000000',
        borderRadius: 2.5,
        alignSelf: 'center',
        marginVertical: 10
    }
})