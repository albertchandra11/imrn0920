console.log (' Nomor 1 \n')

function range(num1,num2){
    var arr = Array();
    if (num1<num2){
        for (var i=num1;i<=num2;i++){
            arr.push(i);
        }
        return arr
    }
    else if (num1>num2){
        for (var i=num1;i>=num2;i--){
            arr.push(i);
        }
        return arr
    }
    else {
        return -1
    }
}
console.log(range(10,1))
console.log(range(1))
console.log(range(11,18))
console.log(range(54,50))
console.log(range())

console.log('\n Nomor 2 \n')
function rangewithStep(num1,num2,step){
    var arr = Array();
    if (num1<num2){
        for (var i=num1;i<=num2;i+=step){
            arr.push(i);
        }
        return arr
    }
    else if (num1>num2){
        for (var i=num1;i>=num2;i-=step){
            arr.push(i);
        }
        return arr
    }
    else {
        return -1
    }
}
console.log(rangewithStep(1,10,2))
console.log(rangewithStep(11,23,3))
console.log(rangewithStep(5,2,1))
console.log(rangewithStep(29,2,4))

console.log('\n Nomor 3 \n')
function sum(start=[null],end=[null],step = 1){
    var jumlah = 0
    if (start<end){
    for (i=start;i<=end;i+=step){
        jumlah +=i
    }
    return jumlah
}
    else{
        for (i=start;i>=end;i-=step){
            jumlah +=i
        }
        return jumlah
    }
}
console.log(sum(1,10))
console.log(sum(5,50,2))
console.log(sum(15,10))
console.log(sum(20,10,2))
console.log(sum(1))
console.log(sum())

console.log('\n Nomor 4 \n')
function dataHandling(array){
    var iD=''
    for (var i=0;i<array.length;i++){
        iD = 'Nomor ID : ' +array[i][0]+ '\nNama : '+array[i][1]+'\nTTL : '+array[i][2]+' '+array[i][3]+'\nHobby : '+array[i][4]+'\n'
        console.log(iD)
    } 
}
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]
dataHandling(input)

console.log('\n Nomor 5 \n')
function balikKata(kata){
    var atak =''
    for (var i=kata.length-1;i>=0;i--){
        atak += kata[i]
    }
    return (atak)
}
console.log(balikKata("Kasur Rusak"))
console.log(balikKata("SanberCode"))
console.log(balikKata("Haji Ijah"))
console.log(balikKata("racecar"))
console.log(balikKata("I am Sanbers"))

console.log('\n Nomor 6 \n')
function dataHandling2(array1){
    array1.splice(1,0,' Elsharawy')
    array1.splice(1,1,array1[2]+array1[1])
    array1.splice(2,1)
    array1.splice(2,1,'Provinsi '+array1[2])
    array1.splice(4,1,'Pria', 'SMA International Metro')
    console.log(array1)
    var split = array1[3].split('/')
    var bulan = split[1]
    switch(bulan){   
        case '01': {console.log(' Januari ');break}
        case '02': {console.log(' Februari ');break}
        case '03': {console.log(' Maret ');break}
        case '04': {console.log(' April ');break}
        case '05': {console.log(' Mei ');break}
        case '06': {console.log(' Juni ');break}
        case '07': {console.log(' Juli ');break}
        case '08': {console.log(' Agustus ');break}
        case '09': {console.log(' September ');break}
        case '10': {console.log(' Oktober ');break}
        case '11': {console.log(' November ');break}
        case '12': {console.log(' Desember ');break}
    }
    bulaN = split.join('-')
    Bulan = split.sort(function(value1,value2){return value2 - value1})
    console.log(Bulan)
    console.log(bulaN)
    Nama = array1[1].slice(0,14)
    console.log(Nama)
}
var input1 = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"]  
dataHandling2(input1)